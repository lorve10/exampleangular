import { Component, OnInit } from '@angular/core';
import { NgModule } from '@angular/core';
import {ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ServicioService } from '../../servicio/servicio.service';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css']
})
export class UpdateComponent implements OnInit {
  categoria: [];
  nombre: string = "";
  id: number;
  form :FormGroup;

  constructor( public service : ServicioService,
                private route: ActivatedRoute, /// esta nos permite estraer la varibale de la ruta que debemos editar
                public router:Router
    ) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    console.log(this.id);

    this.service.find('categoria/',this.id).subscribe(res=>{
      this.categoria = res.data;
      this.nombre = res.data.cat_nombre;
      console.log("aqui respuesta");
      console.log(res);
    })
  }


  guardar(){
    let data = {
      'nombre': this.nombre
    };
    this.service.update('categoria/',this.id, JSON.stringify(data)).subscribe(res => {
         console.log('Person updated successfully!');
         this.router.navigateByUrl('categori/listar');
    })
  }

}
