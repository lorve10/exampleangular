import { Component, OnInit } from '@angular/core';
import { NgModule } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ServicioService } from '../../servicio/servicio.service';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {
  nombre: string = '';

  constructor(public servicio: ServicioService,
              public router:Router ) { }

  ngOnInit(): void {
  }

  guardar(){
    let data = {
      'nombre': this.nombre
    };
    this.servicio.create('categoria/crear', JSON.stringify(data)).subscribe(response=>{
        console.log(response);
        console.log("guardado con exito");
        this.router.navigateByUrl('categori/listar');
    })
  }

}
