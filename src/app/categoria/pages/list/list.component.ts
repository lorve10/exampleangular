import { Component, OnInit } from '@angular/core';
import { ServicioService } from '../../servicio/servicio.service';
@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  public lista: [] = [];

  constructor(public servicio: ServicioService) { }

  ngOnInit( ): void {
    //listando categorias
    this.listar();
  }

  listar(){
    this.servicio.getAll('categoria/get').subscribe(response=>{
      console.log(response);
      this.lista = response;
    })
  }

  eliminar(id){
    let data = {
      'id': id
    };
    this.servicio.create('categoria/delete', JSON.stringify(data)).subscribe(response=>{
        alert("eliminado con exito");
        this.listar();
    })

  }

}
