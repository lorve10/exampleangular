import { Component, OnInit } from '@angular/core';
import { ProductoService } from '../../servicio/producto.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  lista = [];
  public search: string = "";
  public id: number;
  public nombre: string = "";
  public cantidad: string = "";
  public precio: number;


  constructor(public productoService: ProductoService,
              public modal:NgbModal) { }

  ngOnInit(): void {
    this.listando();
  }

  listando(){
    this.productoService.getAll('productos/getPro').subscribe(response => {
      return  this.lista = response;
    })
  }
  onSearch(search:string){
    this.search = search;
  }

  entrada(id){
    this.id = id;
    this.productoService.find('productos/',this.id).subscribe(response=>{
      let data = response.data;
      this.nombre = data.nombre;
    })
  }

  salida(id){
    this.id = id;
    this.productoService.find('productos/',this.id).subscribe(response=>{
      let data = response.data;
      this.nombre = data.nombre;
    })
  }
  limpiar(){
    console.log("entrooo");
    this.id = 0;
    this.nombre = "";
    this.cantidad = "";
    this.precio = 0;
  }
  guardar(){
    let data = {
      'cantidad': this.cantidad,
      'precio': this.precio
    }
    this.productoService.update('productos/',this.id, JSON.stringify(data)).subscribe(res => {
         alert('producto updated successfully!');
         this.listando();
         this.limpiar();
    })
  }

  guardarSalida(){
    let data = {
      'cantidad': this.cantidad,
    }
    this.productoService.update('productos/salida/',this.id, JSON.stringify(data)).subscribe(res => {
      if(res.success){
        alert("Salida con exito")
        this.listando();
        this.limpiar();
      }else{
        alert("la cantidad que solicitaste es mayor a la actual");
      }
    })
  }



}
