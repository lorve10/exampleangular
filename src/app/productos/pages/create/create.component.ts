import { Component, OnInit } from '@angular/core';
import { NgModule } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ProductoService } from '../../servicio/producto.service';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {
  categori = [];
  proveedor = [];
  /// variables
  nombre: string = '';
  categoria: string = '';
  provee: string = '';
  precio: string = '';
  cantidad: string = '';
  show = false;
  list = {
    nombre: '',
    proveedor: '',
    categoria: '',
    precio: '',
    cantidad: ''
  };
  lista = [];

  constructor( public productoService : ProductoService,
              private router: Router
  ) {   }

  ngOnInit(): void {
    //consulta categoria
    this.listCat()
    // consulta proveedores
    this.listProv()
  }
  limpiar(){
    this.nombre= "";
    this.provee= '';
    this.categoria = '';
    this.precio = '';
    this.cantidad = '';

    this.list = {
      nombre: '',
      proveedor: '',
      categoria: '',
      precio: '',
      cantidad: ''
    };
  }
  holaM(){
    const agregar = [];

    this.list.nombre = this.nombre;
    this.list.proveedor = this.provee;
    this.list.categoria = this.categoria;
    this.list.precio = this.precio;
    this.list.cantidad = this.cantidad;
    //  console.log(res);
    this.lista.push(this.list);
    this.limpiar()
    console.log(this.lista);

  }
  listCat(){
    this.productoService.getAll('categoria/get').subscribe(response => {
      console.log(response);
      return  this.categori = response;
    })
  }
  listProv(){
    this.productoService.getAll('proveedor/get').subscribe(response => {
      console.log(response);
      return  this.proveedor = response;
    })
  }

  guardar(){
    let data = {
      'lista': this.lista
    };
    this.productoService.create('productos/save', JSON.stringify(data)).subscribe(response=>{
        console.log(response);
        console.log("guardado con exito");
        this.router.navigateByUrl('product/listar');
    })
  }

  eliminar(data){
    const lista = this.lista.filter(item=>  item !== data);
    console.log(lista);

    this.lista=lista;
    console.log(this.lista);

  }

}
