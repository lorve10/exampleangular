import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductosRoutingModule } from './productos-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { CreateComponent } from './pages/create/create.component';
import { UpdateComponent } from './pages/update/update.component';
import { ListComponent } from './pages/list/list.component';
import { FiltroPipe } from './pipes/filtro.pipe';


@NgModule({
  declarations: [CreateComponent, UpdateComponent, ListComponent, FiltroPipe],
  imports: [
    CommonModule,
    FormsModule,
    ProductosRoutingModule
  ]
})
export class ProductosModule { }
