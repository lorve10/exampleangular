export interface Producto {
  id: number;
  nombre: string;
  precio: number;
  cantidad: number;
  id_categoria: number;
  id_proveedor: number;

}
