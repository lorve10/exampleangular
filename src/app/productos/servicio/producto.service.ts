import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment.prod';
import {  Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class ProductoService {

  base = environment.apiUrl;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }
  constructor( public http: HttpClient) { }

  getAll(url:string): Observable<any>{
     return this.http.get(this.base + url)
     .pipe(
       catchError(this.errorHandler)
     )
   }

   create(url:string, data ): Observable<any> {
     return this.http.post(this.base + url ,data, this.httpOptions)
     .pipe(
       catchError(this.errorHandler)
     )
   }

   find(url:string, id): Observable<any> {
     return this.http.get(this.base+ url + id, this.httpOptions)
     .pipe(
       catchError(this.errorHandler)
     )
   }

   update(url:string, id, data): Observable<any> {
     return this.http.put(this.base + url + id, data, this.httpOptions)
     .pipe(
       catchError(this.errorHandler)
     )
   }

   errorHandler(error) {
     let errorMessage = '';
     if(error.error instanceof ErrorEvent) {
       errorMessage = error.error.message;
     } else {
       errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
     }
     return throwError(errorMessage);
   }

}
