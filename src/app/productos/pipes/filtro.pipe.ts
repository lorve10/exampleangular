import { Pipe, PipeTransform } from '@angular/core';
import { Producto } from '../producto';
@Pipe({
  name: 'filtro'
})
export class FiltroPipe implements PipeTransform {
  oneSearch = [];
  transform( producto : Producto[], search: string = ''): Producto[] {
      if (search.length === 0) {
        return producto.slice(0,5)
      }
      const filtroSearch =  producto.filter(item => item.nombre.toLowerCase().indexOf(search) !== -1);
      return filtroSearch;
  }

}
