import { Component, OnInit } from '@angular/core';
import { NgModule } from '@angular/core';
import {ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ServicioService } from '../../servicio/servicio.service';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css']
})
export class UpdateComponent implements OnInit {
  id: number;
  nombre: string = "";
  telefono : string = "";
  direccion: string = "";

  constructor(public servicio: ServicioService,
              public route: ActivatedRoute,
              public router: Router) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    console.log(this.id);

    this.servicio.find('proveedor/',this.id).subscribe(res=>{
        console.log(res.data);
        let data = res.data;
        this.nombre = data.pro_nombre;
        this.telefono = data.pro_telefono;
        this.direccion = data.pro_direccion;
    })
  }


  guardar(){
    const data ={
      'nombre': this.nombre,
      'direccion': this.direccion,
      'telefono': this.telefono
    }
    this.servicio.update('proveedor/',this.id, JSON.stringify(data)).subscribe(resp=>{
      alert("Editado con exito");
      this.router.navigateByUrl('/proveedor/listar');
    })



  }



}
