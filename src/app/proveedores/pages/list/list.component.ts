import { Component, OnInit } from '@angular/core';
import { NgModule } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ServicioService } from '../../servicio/servicio.service';


@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  lista: [];
  constructor( public servicio: ServicioService) { }

  ngOnInit(): void {
    this.listProv(); //lista de proveedores
  }


  listProv(){
    this.servicio.getAll('proveedor/get').subscribe(response => {
      console.log(response);
      return  this.lista = response;
    })
  }

  eliminar(id){
    let data = {
      'id': id
    };
    this.servicio.create('proveedor/delete', JSON.stringify(data)).subscribe(response=>{
      console.log(response);

        if (response.success) {
          alert("eliminado con exito");
          this.listProv();
        }else{
          alert("error al eliminar")
        }

    })

  }

}
