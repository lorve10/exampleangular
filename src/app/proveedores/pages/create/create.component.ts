import { Component, OnInit } from '@angular/core';
import { NgModule } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ServicioService } from '../../servicio/servicio.service';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {
  nombre: string ="";
  telefono: string = "";
  direccion: string = "";

  constructor(public servicio: ServicioService, public router: Router) { }

  ngOnInit(): void {
  }

  guardar(){
      const data = {
        'nombre': this.nombre,
        'telefono': this.telefono,
        'direccion': this.telefono
      }
      this.servicio.create('proveedor/save', JSON.stringify(data)).subscribe(res=>{
        this.router.navigateByUrl('proveedor');

        console.log(res);
      })
  }


}
