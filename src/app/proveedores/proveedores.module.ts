import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProveedoresRoutingModule } from './proveedores-routing.module';
import { CreateComponent } from './pages/create/create.component';
import { ListComponent } from './pages/list/list.component';
import { UpdateComponent } from './pages/update/update.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [ CreateComponent, ListComponent, UpdateComponent],
  imports: [
    CommonModule,
    ProveedoresRoutingModule,
    FormsModule
  ]
})
export class ProveedoresModule { }
