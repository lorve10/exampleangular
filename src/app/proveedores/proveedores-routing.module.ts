import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateComponent } from './pages/create/create.component';
import { ListComponent } from './pages/list/list.component';
import { UpdateComponent } from './pages/update/update.component';

const routes: Routes = [{
  path:'',
  children:[
    { path:'agregar', component: CreateComponent },
    { path:'editar/:id', component:  UpdateComponent },
    { path:'listar', component:  ListComponent },
    { path:'**', redirectTo: 'listar' }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProveedoresRoutingModule { }
