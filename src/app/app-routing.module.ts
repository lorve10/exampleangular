import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'auth',
    loadChildren: ()=> import('./auth/auth.module').then(m=> m.AuthModule)
  },
  {
    path: 'product',
    loadChildren: ()=> import('./productos/productos.module').then(m=> m.ProductosModule)
  },
  {
    path: 'categori',
    loadChildren: ()=> import('./categoria/categoria.module').then(m=> m.CategoriaModule)
  },
  {
    path: 'proveedor',
    loadChildren: ()=> import('./proveedores/proveedores.module').then(m=> m.ProveedoresModule)
  },
  {
    path: '**',
    redirectTo:  'auth',
  }
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
